from org.csstudio.display.builder.runtime.script import PVUtil

prefix = PVUtil.getString(pvs[0])

# if is a down-sampled or PIERR channel get DAQ format from PV
if len(prefix.split("Dwn")) > 1 or ("RFCErr" in prefix) :
    if len(channel.split("Dwn")) > 1:
        pv_daq = PVUtil.createPV(prefix + channel + "-Fmt-RB", 5000)
    else:
        pv_daq = PVUtil.createPV(prefix + channel + "Fmt-RB", 5000) #Naming different for PIERR
    daqfmt = PVUtil.getString(pv_daq)
else: #otherwise the default format is IQ
    daqfmt = "I/Q"
widget.getPropertyValue("macros").add("CMP0", daqfmt.split("/")[0])
if len(daqfmt.split("/")) > 1:
    widget.getPropertyValue("macros").add("CMP1", daqfmt.split("/")[1])
else:
    widget.getPropertyValue("macros").add("CMP1", "NONE")


if len(channel.split("IntCh")) > 1:
    widget.getPropertyValue("macros").add("AXSEP", "")
else:
    widget.getPropertyValue("macros").add("AXSEP", "-")

widget.setPropertyValue("file", "full-plot-emb.bob")
