from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from jarray import array
import csv

run = PVUtil.getInt(pvs[0])
if run == 1:
    file_name = PVUtil.getString(pvs[1])
    col1 = PVUtil.getDoubleArray(pvs[2])
    col2 = PVUtil.getDoubleArray(pvs[3])
    with open(file_name, "w") as csvfile:
        writer = csv.writer(csvfile)
        for i in range(len(col1)):
            writer.writerow([col1[i], col2[i]])

    pvs[0].setValue(0)
