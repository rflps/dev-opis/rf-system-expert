def get():
    _rflps_macros_DTL_010 = {}
    _rflps_macros_DTL_010['FIM_MACROS']='scripts/fim_macros_dtl_010.py'
    _rflps_macros_DTL_010['IOCSTATS_FIM']='DTL-010:Ctrl-IOC-101:Meta'
    _rflps_macros_DTL_010['IOCSTATS_SIM']='DTL-RFLPS1:Ctrl-IOC-001'
    _rflps_macros_DTL_010['PREFIX']='DTL-010'
    _rflps_macros_DTL_010['path']='../rflps/01_mainDTLCPI.bob'
    return _rflps_macros_DTL_010
