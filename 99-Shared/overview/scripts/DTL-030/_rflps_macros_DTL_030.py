def get():
    _rflps_macros_DTL_030 = {}
    _rflps_macros_DTL_030['FIM_MACROS']='scripts/fim_macros_dtl_030.py'
    _rflps_macros_DTL_030['IOCSTATS_FIM']='DTL-RFLPS3:Ctrl-IOC-002'
    _rflps_macros_DTL_030['IOCSTATS_SIM']='DTL-RFLPS3:Ctrl-IOC-001'
    _rflps_macros_DTL_030['P']='DTL-030:'
    _rflps_macros_DTL_030['PREFIX']='DTL-030'
    _rflps_macros_DTL_030['path']='../rflps/01_mainDTLTHALES.bob'
    return _rflps_macros_DTL_030
