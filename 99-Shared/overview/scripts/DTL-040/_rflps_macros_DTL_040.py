def get():
    _rflps_macros_DTL_040 = {}
    _rflps_macros_DTL_040['FIM_MACROS']='scripts/fim_macros_dtl_040.py'
    _rflps_macros_DTL_040['IOCSTATS_FIM']='DTL-RFLPS4:Ctrl-IOC-002'
    _rflps_macros_DTL_040['IOCSTATS_SIM']='DTL-RFLPS4:Ctrl-IOC-001'
    _rflps_macros_DTL_040['P']='DTL-040:'
    _rflps_macros_DTL_040['PREFIX']='DTL-040'
    _rflps_macros_DTL_040['path']='../rflps/01_mainDTLTHALES.bob'
    return _rflps_macros_DTL_040
