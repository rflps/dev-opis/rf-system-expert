def get():
    _rflps_macros_DTL_050 = {}
    _rflps_macros_DTL_050['FIM_MACROS']='scripts/fim_macros_dtl_050.py'
    _rflps_macros_DTL_050['IOCSTATS_FIM']='DTL-RFLPS5:Ctrl-IOC-002'
    _rflps_macros_DTL_050['IOCSTATS_SIM']='DTL-RFLPS5:Ctrl-IOC-001'
    _rflps_macros_DTL_050['P']='DTL-050:'
    _rflps_macros_DTL_050['PREFIX']='DTL-050'
    _rflps_macros_DTL_050['path']='../rflps/01_mainDTLTHALES.bob'
    return _rflps_macros_DTL_050
