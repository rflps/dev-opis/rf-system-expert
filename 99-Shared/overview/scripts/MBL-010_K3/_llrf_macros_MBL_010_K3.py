def get():
    _llrf_macros_MBL_010_K3 = {}
    _llrf_macros_MBL_010_K3['CAVFLDDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_010_K3['HAS2']='true'
    _llrf_macros_MBL_010_K3['P']='RFQ-LLRF1::'
    _llrf_macros_MBL_010_K3['PC']='$(PD)'
    _llrf_macros_MBL_010_K3['PD']='RFQ-010:'
    _llrf_macros_MBL_010_K3['PR']='RFQ-010:'
    _llrf_macros_MBL_010_K3['PREAMPCH']='0'
    _llrf_macros_MBL_010_K3['PREAMPDIG']='$(PD)$(RD2)'
    _llrf_macros_MBL_010_K3['PWRAMPCH']='6'
    _llrf_macros_MBL_010_K3['PWRAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_010_K3['RC']='$(RD1)'
    _llrf_macros_MBL_010_K3['RD']='RFS-DIG-101:'
    _llrf_macros_MBL_010_K3['RD1']='RFS-DIG-101:'
    _llrf_macros_MBL_010_K3['RD2']='RFS-DIG-102:'
    _llrf_macros_MBL_010_K3['RD3']='RFS-DIG-103:'
    _llrf_macros_MBL_010_K3['RD4']='RFS-DIG-104:'
    _llrf_macros_MBL_010_K3['RR']='RFS-RFM-101:'
    _llrf_macros_MBL_010_K3['RR1']='RFS-RFM-101:'
    _llrf_macros_MBL_010_K3['RR2']='RFS-RFM-102:'
    _llrf_macros_MBL_010_K3['RR3']='RFS-RFM-103:'
    _llrf_macros_MBL_010_K3['RR4']='RFS-RFM-104:'
    _llrf_macros_MBL_010_K3['SYSDESC']='Overview'
    _llrf_macros_MBL_010_K3['SYSNAME']='RFQ'
    _llrf_macros_MBL_010_K3['VMDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_010_K3['path']='../llrf/overview.bob'
    return _llrf_macros_MBL_010_K3
