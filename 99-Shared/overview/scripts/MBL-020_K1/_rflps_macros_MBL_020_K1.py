def get():
    _rflps_macros_MBL_020_K1 = {}
    _rflps_macros_MBL_020_K1['FIM_MACROS']='scripts/fim_macros_mbl020_1.py'
    _rflps_macros_MBL_020_K1['IOCSTATS_FIM']='MBL-020RFC:Ctrl-IOC-104'
    _rflps_macros_MBL_020_K1['IOCSTATS_SIM']='MBL-020RFC:Ctrl-IOC-103'
    _rflps_macros_MBL_020_K1['KLY']='1'
    _rflps_macros_MBL_020_K1['P']='MBL-020RFC'
    _rflps_macros_MBL_020_K1['PREFIX']='MBL-020RFC'
    _rflps_macros_MBL_020_K1['path']='../rflps/01_mainMBLCanon.bob'
    return _rflps_macros_MBL_020_K1
