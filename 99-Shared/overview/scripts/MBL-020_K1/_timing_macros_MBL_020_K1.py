def get():
    _timing_macros_MBL_020_K1 = {}
    _timing_macros_MBL_020_K1['EVRPREFIX']='MBL-020RFC:RFS-EVR-101:'
    _timing_macros_MBL_020_K1['PREFIX']='MBL-020RFC:RFS-EVR-101:'
    _timing_macros_MBL_020_K1['SYSNAME']='MBL-020 K1'
    _timing_macros_MBL_020_K1['path']='../timing/timing-local.bob'
    return _timing_macros_MBL_020_K1
