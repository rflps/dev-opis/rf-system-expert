def get():
    _llrf_macros_MBL_020_K2 = {}
    _llrf_macros_MBL_020_K2['P']='MBL-020RFC:RFS-LLRF-201:'
    _llrf_macros_MBL_020_K2['PC']='$(PD)'
    _llrf_macros_MBL_020_K2['PD']='MBL-020RFC:'
    _llrf_macros_MBL_020_K2['PR']='MBL-020RFC:'
    _llrf_macros_MBL_020_K2['PREAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_020_K2['PWRAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_020_K2['RC']='$(RD1)'
    _llrf_macros_MBL_020_K2['RD']='RFS-DIG-201:'
    _llrf_macros_MBL_020_K2['RD1']='RFS-DIG-201:'
    _llrf_macros_MBL_020_K2['RR']='RFS-RFM-201:'
    _llrf_macros_MBL_020_K2['RR1']='RFS-RFM-201:'
    _llrf_macros_MBL_020_K2['SYSDESC']='Overview'
    _llrf_macros_MBL_020_K2['SYSNAME']='MBL-020 K2'
    _llrf_macros_MBL_020_K2['VMDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_020_K2['path']='../llrf/overview.bob'
    return _llrf_macros_MBL_020_K2
