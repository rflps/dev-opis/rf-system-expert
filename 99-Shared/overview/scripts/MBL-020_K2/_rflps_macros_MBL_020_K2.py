def get():
    _rflps_macros_MBL_020_K2 = {}
    _rflps_macros_MBL_020_K2['FIM_MACROS']='scripts/fim_macros_mbl020_2.py'
    _rflps_macros_MBL_020_K2['IOCSTATS_FIM']='MBL-020RFC:Ctrl-IOC-204'
    _rflps_macros_MBL_020_K2['IOCSTATS_SIM']='MBL-020RFC:Ctrl-IOC-203'
    _rflps_macros_MBL_020_K2['KLY']='2'
    _rflps_macros_MBL_020_K2['P']='MBL-020RFC'
    _rflps_macros_MBL_020_K2['KLYSTRON_NAME']='MBL-020-S1'
    _rflps_macros_MBL_020_K2['PREFIX']='MBL-020RFC'
    _rflps_macros_MBL_020_K2['path']='../rflps/01_mainMBLCanon.bob'
    return _rflps_macros_MBL_020_K2
