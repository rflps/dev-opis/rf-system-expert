def get():
    _timing_macros_MBL_020_K2 = {}
    _timing_macros_MBL_020_K2['EVRPREFIX']='MBL-020RFC:RFS-EVR-201:'
    _timing_macros_MBL_020_K2['PREFIX']='MBL-020RFC:RFS-EVR-201:'
    _timing_macros_MBL_020_K2['SYSNAME']='MBL-020 K2'
    _timing_macros_MBL_020_K2['path']='../timing/timing-local.bob'
    return _timing_macros_MBL_020_K2
