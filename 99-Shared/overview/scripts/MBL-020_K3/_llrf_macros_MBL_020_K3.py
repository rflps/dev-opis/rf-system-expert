def get():
    _llrf_macros_MBL_020_K3 = {}
    _llrf_macros_MBL_020_K3['P']='MBL-020RFC:RFS-LLRF-301:'
    _llrf_macros_MBL_020_K3['PC']='$(PD)'
    _llrf_macros_MBL_020_K3['PD']='MBL-020RFC:'
    _llrf_macros_MBL_020_K3['PR']='MBL-020RFC:'
    _llrf_macros_MBL_020_K3['PREAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_020_K3['PWRAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_020_K3['RC']='$(RD1)'
    _llrf_macros_MBL_020_K3['RD']='RFS-DIG-301:'
    _llrf_macros_MBL_020_K3['RD1']='RFS-DIG-301:'
    _llrf_macros_MBL_020_K3['RR']='RFS-RFM-301:'
    _llrf_macros_MBL_020_K3['RR1']='RFS-RFM-301:'
    _llrf_macros_MBL_020_K3['SYSDESC']='Overview'
    _llrf_macros_MBL_020_K3['SYSNAME']='MBL-020 K3'
    _llrf_macros_MBL_020_K3['VMDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_020_K3['path']='../llrf/overview.bob'
    return _llrf_macros_MBL_020_K3
