def get():
    _timing_macros_MBL_020_K3 = {}
    _timing_macros_MBL_020_K3['EVRPREFIX']='MBL-020RFC:RFS-EVR-301:'
    _timing_macros_MBL_020_K3['PREFIX']='MBL-020RFC:RFS-EVR-301:'
    _timing_macros_MBL_020_K3['SYSNAME']='MBL-020 K3'
    _timing_macros_MBL_020_K3['path']='../timing/timing-local.bob'
    return _timing_macros_MBL_020_K3
