def get():
    _rflps_macros_MBL_020_K4 = {}
    _rflps_macros_MBL_020_K4['FIM_MACROS']='scripts/fim_macros_mbl020_4.py'
    _rflps_macros_MBL_020_K4['IOCSTATS_FIM']='MBL-020RFC:Ctrl-IOC-404'
    _rflps_macros_MBL_020_K4['IOCSTATS_SIM']='MBL-020RFC:Ctrl-IOC-403'
    _rflps_macros_MBL_020_K4['KLY']='4'
    _rflps_macros_MBL_020_K4['P']='MBL-020RFC'
    _rflps_macros_MBL_020_K4['PREFIX']='MBL-020RFC'
    _rflps_macros_MBL_020_K4['path']='../rflps/01_mainMBLCanon.bob'
    return _rflps_macros_MBL_020_K4
