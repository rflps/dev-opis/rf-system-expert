def get():
    _klystron_macros_MBL_030_K1 = {}
    _klystron_macros_MBL_030_K1['P']='MBL-030RFC:'
    _klystron_macros_MBL_030_K1['R']='RFS-Kly-110:'
    _klystron_macros_MBL_030_K1['path']='../klystron/klystron_info.bob'
    return _klystron_macros_MBL_030_K1
