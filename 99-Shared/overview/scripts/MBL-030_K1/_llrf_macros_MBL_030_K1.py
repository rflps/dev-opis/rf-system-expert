def get():
    _llrf_macros_MBL_030_K1 = {}
    _llrf_macros_MBL_030_K1['P']='MBL-030RFC:RFS-LLRF-101:'
    _llrf_macros_MBL_030_K1['PC']='$(PD)'
    _llrf_macros_MBL_030_K1['PD']='MBL-030RFC:'
    _llrf_macros_MBL_030_K1['PR']='MBL-030RFC:'
    _llrf_macros_MBL_030_K1['PREAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_030_K1['PWRAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_030_K1['RC']='$(RD1)'
    _llrf_macros_MBL_030_K1['RD']='RFS-DIG-101:'
    _llrf_macros_MBL_030_K1['RD1']='RFS-DIG-101:'
    _llrf_macros_MBL_030_K1['RR']='RFS-RFM-101:'
    _llrf_macros_MBL_030_K1['RR1']='RFS-RFM-101:'
    _llrf_macros_MBL_030_K1['SYSDESC']='Overview'
    _llrf_macros_MBL_030_K1['SYSNAME']='MBL-030 K1'
    _llrf_macros_MBL_030_K1['VMDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_030_K1['path']='../llrf/overview.bob'
    return _llrf_macros_MBL_030_K1
