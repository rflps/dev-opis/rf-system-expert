def get():
    _rflps_macros_MBL_030_K1 = {}
    _rflps_macros_MBL_030_K1['FIM_MACROS']='scripts/fim_macros_mbl030_1.py'
    _rflps_macros_MBL_030_K1['IOCSTATS_FIM']='MBL-030RFC:Ctrl-IOC-104'
    _rflps_macros_MBL_030_K1['IOCSTATS_SIM']='MBL-030RFC:Ctrl-IOC-103'
    _rflps_macros_MBL_030_K1['KLY']='1'
    _rflps_macros_MBL_030_K1['P']='MBL-030RFC'
    _rflps_macros_MBL_030_K1['PREFIX']='MBL-030RFC'
    _rflps_macros_MBL_030_K1['path']='../rflps/01_mainMBL.bob'
    return _rflps_macros_MBL_030_K1
