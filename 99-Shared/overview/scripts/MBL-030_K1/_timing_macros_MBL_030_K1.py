def get():
    _timing_macros_MBL_030_K1 = {}
    _timing_macros_MBL_030_K1['EVRPREFIX']='MBL-030RFC:RFS-EVR-101:'
    _timing_macros_MBL_030_K1['PREFIX']='MBL-030RFC:RFS-EVR-101:'
    _timing_macros_MBL_030_K1['SYSNAME']='MBL-030 K1'
    _timing_macros_MBL_030_K1['path']='../timing/timing-local.bob'
    return _timing_macros_MBL_030_K1
