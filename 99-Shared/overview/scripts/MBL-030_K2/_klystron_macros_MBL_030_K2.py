def get():
    _klystron_macros_MBL_030_K2 = {}
    _klystron_macros_MBL_030_K2['P']='MBL-030RFC:'
    _klystron_macros_MBL_030_K2['R']='RFS-Kly-210:'
    _klystron_macros_MBL_030_K2['path']='../klystron/klystron_info.bob'
    return _klystron_macros_MBL_030_K2
