def get():
    _rflps_macros_MBL_030_K2 = {}
    _rflps_macros_MBL_030_K2['FIM_MACROS']='scripts/fim_macros_mbl030_2.py'
    _rflps_macros_MBL_030_K2['IOCSTATS_FIM']='MBL-030RFC:Ctrl-IOC-204'
    _rflps_macros_MBL_030_K2['IOCSTATS_SIM']='MBL-030RFC:Ctrl-IOC-203'
    _rflps_macros_MBL_030_K2['KLY']='2'
    _rflps_macros_MBL_030_K2['P']='MBL-030RFC'
    _rflps_macros_MBL_030_K2['PREFIX']='MBL-030RFC'
    _rflps_macros_MBL_030_K2['path']='../rflps/01_mainMBL.bob'
    return _rflps_macros_MBL_030_K2
