def get():
    _timing_macros_MBL_030_K2 = {}
    _timing_macros_MBL_030_K2['EVRPREFIX']='MBL-030RFC:RFS-EVR-201:'
    _timing_macros_MBL_030_K2['PREFIX']='MBL-030RFC:RFS-EVR-201:'
    _timing_macros_MBL_030_K2['SYSNAME']='MBL-030 K2'
    _timing_macros_MBL_030_K2['path']='../timing/timing-local.bob'
    return _timing_macros_MBL_030_K2
