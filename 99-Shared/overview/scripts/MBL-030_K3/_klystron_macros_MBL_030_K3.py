def get():
    _klystron_macros_MBL_030_K3 = {}
    _klystron_macros_MBL_030_K3['P']='MBL-030RFC:'
    _klystron_macros_MBL_030_K3['R']='RFS-Kly-310:'
    _klystron_macros_MBL_030_K3['path']='../klystron/klystron_info.bob'
    return _klystron_macros_MBL_030_K3
