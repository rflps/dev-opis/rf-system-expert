def get():
    _rflps_macros_MBL_030_K3 = {}
    _rflps_macros_MBL_030_K3['FIM_MACROS']='scripts/fim_macros_mbl030_3.py'
    _rflps_macros_MBL_030_K3['IOCSTATS_FIM']='MBL-030RFC:Ctrl-IOC-304'
    _rflps_macros_MBL_030_K3['IOCSTATS_SIM']='MBL-030RFC:Ctrl-IOC-303'
    _rflps_macros_MBL_030_K3['KLY']='3'
    _rflps_macros_MBL_030_K3['P']='MBL-030RFC'
    _rflps_macros_MBL_030_K3['PREFIX']='MBL-030RFC'
    _rflps_macros_MBL_030_K3['path']='../rflps/01_mainMBL.bob'
    return _rflps_macros_MBL_030_K3
