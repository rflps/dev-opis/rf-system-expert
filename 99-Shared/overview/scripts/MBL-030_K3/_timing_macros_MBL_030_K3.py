def get():
    _timing_macros_MBL_030_K3 = {}
    _timing_macros_MBL_030_K3['EVRPREFIX']='MBL-030RFC:RFS-EVR-301:'
    _timing_macros_MBL_030_K3['PREFIX']='MBL-030RFC:RFS-EVR-301:'
    _timing_macros_MBL_030_K3['SYSNAME']='MBL-030 K3'
    _timing_macros_MBL_030_K3['path']='../timing/timing-local.bob'
    return _timing_macros_MBL_030_K3
