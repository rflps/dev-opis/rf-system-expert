def get():
    _klystron_macros_MBL_030_K4 = {}
    _klystron_macros_MBL_030_K4['P']='MBL-030RFC:'
    _klystron_macros_MBL_030_K4['R']='RFS-Kly-410:'
    _klystron_macros_MBL_030_K4['path']='../klystron/klystron_info.bob'
    return _klystron_macros_MBL_030_K4
