def get():
    _llrf_macros_MBL_030_K4 = {}
    _llrf_macros_MBL_030_K4['P']='MBL-030RFC:RFS-LLRF-401:'
    _llrf_macros_MBL_030_K4['PC']='$(PD)'
    _llrf_macros_MBL_030_K4['PD']='MBL-030RFC:'
    _llrf_macros_MBL_030_K4['PR']='MBL-030RFC:'
    _llrf_macros_MBL_030_K4['PREAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_030_K4['PWRAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_030_K4['RC']='$(RD1)'
    _llrf_macros_MBL_030_K4['RD']='RFS-DIG-401:'
    _llrf_macros_MBL_030_K4['RD1']='RFS-DIG-401:'
    _llrf_macros_MBL_030_K4['RR']='RFS-RFM-401:'
    _llrf_macros_MBL_030_K4['RR1']='RFS-RFM-401:'
    _llrf_macros_MBL_030_K4['SYSDESC']='Overview'
    _llrf_macros_MBL_030_K4['SYSNAME']='MBL-030 K4'
    _llrf_macros_MBL_030_K4['VMDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_030_K4['path']='../llrf/overview.bob'
    return _llrf_macros_MBL_030_K4
