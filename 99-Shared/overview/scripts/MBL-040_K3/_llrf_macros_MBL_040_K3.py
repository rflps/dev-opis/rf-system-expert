def get():
    _llrf_macros_MBL_040_K3 = {}
    _llrf_macros_MBL_040_K3['P']='MBL-030RFC:RFS-LLRF-401:'
    _llrf_macros_MBL_040_K3['PC']='$(PD)'
    _llrf_macros_MBL_040_K3['PD']='MBL-030RFC:'
    _llrf_macros_MBL_040_K3['PR']='MBL-030RFC:'
    _llrf_macros_MBL_040_K3['PREAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_040_K3['PWRAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_040_K3['RC']='$(RD1)'
    _llrf_macros_MBL_040_K3['RD']='RFS-DIG-401:'
    _llrf_macros_MBL_040_K3['RD1']='RFS-DIG-401:'
    _llrf_macros_MBL_040_K3['RR']='RFS-RFM-401:'
    _llrf_macros_MBL_040_K3['RR1']='RFS-RFM-401:'
    _llrf_macros_MBL_040_K3['SYSDESC']='Overview'
    _llrf_macros_MBL_040_K3['SYSNAME']='MBL-030 K4'
    _llrf_macros_MBL_040_K3['VMDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_040_K3['path']='../llrf/overview.bob'
    return _llrf_macros_MBL_040_K3
