def get():
    _llrf_macros_MBL_050_K2 = {}
    _llrf_macros_MBL_050_K2['P']='MBL-030RFC:RFS-LLRF-401:'
    _llrf_macros_MBL_050_K2['PC']='$(PD)'
    _llrf_macros_MBL_050_K2['PD']='MBL-030RFC:'
    _llrf_macros_MBL_050_K2['PR']='MBL-030RFC:'
    _llrf_macros_MBL_050_K2['PREAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_050_K2['PWRAMPDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_050_K2['RC']='$(RD1)'
    _llrf_macros_MBL_050_K2['RD']='RFS-DIG-401:'
    _llrf_macros_MBL_050_K2['RD1']='RFS-DIG-401:'
    _llrf_macros_MBL_050_K2['RR']='RFS-RFM-401:'
    _llrf_macros_MBL_050_K2['RR1']='RFS-RFM-401:'
    _llrf_macros_MBL_050_K2['SYSDESC']='Overview'
    _llrf_macros_MBL_050_K2['SYSNAME']='MBL-030 K4'
    _llrf_macros_MBL_050_K2['VMDIG']='$(PD)$(RD1)'
    _llrf_macros_MBL_050_K2['path']='../llrf/overview.bob'
    return _llrf_macros_MBL_050_K2
