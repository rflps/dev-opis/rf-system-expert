import _rflps_macros_RFQ
def get():
    macros_overview = {}
    for k,v in _rflps_macros_RFQ.get().items():
        macros_overview['LPS_{}'.format(k)]=v
    macros_overview['CAV_LPS_LINK']='true'
    macros_overview['CAV_LPS_OPI']='/opt/opis/20-SystemExpert/10-ACC/030-RFQ/10-Top/TOP.bob'
    macros_overview['LPSOPI']='01_mainRFQ010.bob'
    macros_overview['P']='$(prefix):'
    macros_overview['PSSWG']='KG-NCG:PSS-Area-1:RFQ_RWG_Removed'
    macros_overview['R']='RFS-FIM-101:'
    macros_overview['SKIDOPI']='cws03-rfq-skid/Top.bob'
    macros_overview['TIM']='RFS-EVR-101:'
    macros_overview['kly']='RFS-Kly-110:'
    macros_overview['mod']='$(P)RFS-Mod-001:'
    macros_overview['modHV']='$(P)RFS-Mod-110:'
    macros_overview['modKly1']='$(P)RFS-Mod-110:'
    macros_overview['modKly2']='DTL-010:RFS-Mod-110:'
    macros_overview['prefix']='RFQ-010'
    macros_overview['skid']='CWM-CWS03:'
    macros_overview['path']='../rflps_overview/horizontal_klystron.bob'
    return macros_overview
