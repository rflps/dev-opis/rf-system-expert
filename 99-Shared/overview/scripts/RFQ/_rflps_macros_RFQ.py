def get():
    _rflps_macros_RFQ = {}
    _rflps_macros_RFQ['FIM_MACROS']='scripts/fim_macros_rfq_01.py'
    _rflps_macros_RFQ['IOCSTATS_FIM']='RFQ-010:Ctrl-IOC-101:Meta'
    _rflps_macros_RFQ['IOCSTATS_SIM']='RFQ-RFLPS1:Ctrl-IOC-001'
    _rflps_macros_RFQ['P']='RFQ-010'
    _rflps_macros_RFQ['PREFIX']='RFQ-010'
    _rflps_macros_RFQ['path']='../rflps/01_mainRFQ010.bob'
    return _rflps_macros_RFQ
