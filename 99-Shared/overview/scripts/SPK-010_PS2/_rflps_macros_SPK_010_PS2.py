def get():
    _rflps_macros_SPK_010_PS2 = {}
    _rflps_macros_SPK_010_PS2['FIM_MACROS']='scripts/fim_macros_spk_04.py'
    _rflps_macros_SPK_010_PS2['IOCSTATS_FIM']='SPK-010:Ctrl-IOC-102:Meta'
    _rflps_macros_SPK_010_PS2['IOCSTATS_SIM']='SPK-010-RFLPS1:Ctrl-IOC-102'
    _rflps_macros_SPK_010_PS2['KLY']='2'
    _rflps_macros_SPK_010_PS2['P']='SPK-010'
    _rflps_macros_SPK_010_PS2['PREFIX']='SPK-010'
    _rflps_macros_SPK_010_PS2['path']='../rflps/01_mainSPK.bob'
    return _rflps_macros_SPK_010_PS2
