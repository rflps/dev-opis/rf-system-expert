def get():
    _rflps_macros_SPK_020_PS1 = {}
    _rflps_macros_SPK_020_PS1['FIM_MACROS']='scripts/fim_macros_spk_04.py'
    _rflps_macros_SPK_020_PS1['IOCSTATS_FIM']='SPK-020:Ctrl-IOC-102:Meta'
    _rflps_macros_SPK_020_PS1['IOCSTATS_SIM']='SPK-020-RFLPS1:Ctrl-IOC-102'
    _rflps_macros_SPK_020_PS1['KLY']='1'
    _rflps_macros_SPK_020_PS1['P']='SPK-020'
    _rflps_macros_SPK_020_PS1['PREFIX']='SPK-020'
    _rflps_macros_SPK_020_PS1['path']='../rflps/01_mainSPK.bob'
    return _rflps_macros_SPK_020_PS1
