from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.phoebus.framework.macros import MacroHandler

# Section and subsection are fixed per script
macros = {}

##################################################################################################
#	Create the list of all buttons
##################################################################################################

buttons_lst = [ 'LPSdetailed',
                'LPSoverview',
                'TimingLocalMode',
                'LLRF',
                'KlyInfo',
                'SignalConditioning',
                'ElectronPickup',
                'PinDiode',
                'LocalOscillator',
                'Preamp',
                'Solenoid',
                'IonPump',
                'FilamentPower',
                'Circulator',
                'ArcDetectors',
                'RFoverview',
                'FullOverview']

# Define enabled and disabled buttons
btns_enabled = {}
for btn in buttons_lst:
    btns_enabled[btn]='true'

## Buttons Disabled
btns_enabled['KlyInfo']='false'
btns_enabled['LPSoverview']='false'
btns_enabled['SignalConditioning']='false'
btns_enabled['ElectronPickup']='false'
btns_enabled['PinDiode']='false'
btns_enabled['LocalOscillator']='false'
btns_enabled['Preamp']='false'
btns_enabled['Solenoid']='false'
btns_enabled['IonPump']='false'
btns_enabled['FilamentPower']='false'
btns_enabled['Circulator']='false'
btns_enabled['ArcDetectors']='false'
btns_enabled['RFoverview']='false'
btns_enabled['FullOverview']='false'

##################################################################################################
#	Hardcoded macro data
##################################################################################################

# Macros for the buttons
## Macros for llrf
import _llrf_macros_SPK_030_PS1
macros['LLRF'] = _llrf_macros_SPK_030_PS1.get()

## Macros for timing
import _timing_macros_SPK_030_PS1
macros['TimingLocalMode'] = _timing_macros_SPK_030_PS1.get()

## Macros for rflps
import _rflps_macros_SPK_030_PS1
macros['LPSdetailed'] = _rflps_macros_SPK_030_PS1.get()


##################################################################################################
# Send the macros to each respective widget, or disable the buttons
##################################################################################################

buttons = {}
groups = {}
for btn in buttons_lst:
    buttons['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, '{}_btn'.format(btn))
    groups['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, btn)
for btn,enabled in btns_enabled.items():
    if enabled=='false':
        buttons['{}_button'.format(btn)].setPropertyValue('enabled','false')
    if enabled=='true':
        for k,v in macros[btn].items():
            groups['{}_button'.format(btn)].getPropertyValue('macros').add(k,v)
