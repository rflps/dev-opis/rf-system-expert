def get():
    _rflps_macros_SPK_030_PS1 = {}
    _rflps_macros_SPK_030_PS1['FIM_MACROS']='scripts/fim_macros_spk_04.py'
    _rflps_macros_SPK_030_PS1['IOCSTATS_FIM']='SPK-030:Ctrl-IOC-101:Meta'
    _rflps_macros_SPK_030_PS1['IOCSTATS_SIM']='SPK-030-RFLPS1:Ctrl-IOC-101'
    _rflps_macros_SPK_030_PS1['KLY']='1'
    _rflps_macros_SPK_030_PS1['P']='SPK-030'
    _rflps_macros_SPK_030_PS1['PREFIX']='SPK-030'
    _rflps_macros_SPK_030_PS1['path']='../rflps/01_mainSPKCooling.bob'
    return _rflps_macros_SPK_030_PS1
