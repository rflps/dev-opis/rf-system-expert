def get():
    _rflps_macros_SPK_040_PS2 = {}
    _rflps_macros_SPK_040_PS2['FIM_MACROS']='scripts/fim_macros_spk_04.py'
    _rflps_macros_SPK_040_PS2['IOCSTATS_FIM']='SPK-040:Ctrl-IOC-202:Meta'
    _rflps_macros_SPK_040_PS2['IOCSTATS_SIM']='SPK-040-RFLPS1:Ctrl-IOC-202'
    _rflps_macros_SPK_040_PS2['KLY']='2'
    _rflps_macros_SPK_040_PS2['P']='SPK-040'
    _rflps_macros_SPK_040_PS2['PREFIX']='SPK-040'
    _rflps_macros_SPK_040_PS2['path']='../rflps/01_mainSPK.bob'
    return _rflps_macros_SPK_040_PS2
