def get():
    _rflps_macros_SPK_060_PS1 = {}
    _rflps_macros_SPK_060_PS1['FIM_MACROS']='scripts/fim_macros_spk_03.py'
    _rflps_macros_SPK_060_PS1['IOCSTATS_FIM']='SPK-060:Ctrl-IOC-101:Meta'
    _rflps_macros_SPK_060_PS1['IOCSTATS_SIM']='SPK-060-RFLPS1:Ctrl-IOC-101'
    _rflps_macros_SPK_060_PS1['KLY']='1'
    _rflps_macros_SPK_060_PS1['P']='SPK-060'
    _rflps_macros_SPK_060_PS1['PREFIX']='SPK-060'
    _rflps_macros_SPK_060_PS1['path']='../rflps/01_mainSPK.bob'
    return _rflps_macros_SPK_060_PS1
