def get():
    _rflps_macros_SPK_070_PS1 = {}
    _rflps_macros_SPK_070_PS1['FIM_MACROS']='scripts/fim_macros_spk_01.py'
    _rflps_macros_SPK_070_PS1['IOCSTATS_FIM']='SPK-070:Ctrl-IOC-101:Meta'
    _rflps_macros_SPK_070_PS1['IOCSTATS_SIM']='SPK-070-RFLPS1:Ctrl-IOC-101'
    _rflps_macros_SPK_070_PS1['KLY']='1'
    _rflps_macros_SPK_070_PS1['P']='SPK-070'
    _rflps_macros_SPK_070_PS1['PREFIX']='SPK-070'
    _rflps_macros_SPK_070_PS1['path']='../rflps/01_mainSPKCooling.bob'
    return _rflps_macros_SPK_070_PS1
