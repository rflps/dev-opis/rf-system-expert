def get():
    _rflps_macros_SPK_110_PS2 = {}
    _rflps_macros_SPK_110_PS2['FIM_MACROS']='scripts/fim_macros_spk_04.py'
    _rflps_macros_SPK_110_PS2['IOCSTATS_FIM']='SPK-110:Ctrl-IOC-201:Meta'
    _rflps_macros_SPK_110_PS2['IOCSTATS_SIM']='SPK-110-RFLPS1:Ctrl-IOC-201'
    _rflps_macros_SPK_110_PS2['KLY']='2'
    _rflps_macros_SPK_110_PS2['P']='SPK-110'
    _rflps_macros_SPK_110_PS2['PREFIX']='SPK-110'
    _rflps_macros_SPK_110_PS2['path']='../rflps/01_mainSPK.bob'
    return _rflps_macros_SPK_110_PS2
