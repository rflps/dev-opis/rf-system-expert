from org.csstudio.opibuilder.scriptUtil import PVUtil
 
# This script will populate several macros that will be used to mount the PV names inside the GUIs.
# Version: 2021-10-20
 
##################################################################################################
# Macro data:
##################################################################################################
 
macros = {}
 
macros["P"] = "DTL-040:"
macros["SEC"] = "DTL"
macros["SUB"] = "040"
macros["IOC_"] = "RFS-FIM-101:"
macros["IOCSTATS_"] = "sec_+-RFLPS4:+ioc2_"
macros["AI00"] = "RFS-PAmp-110:PwrFwd-"
macros["AI01"] = "RFS-Kly-110:PwrFwd-"
macros["AI02"] = "RFS-Load-130:PwrFwd-"
macros["AI03"] = "RFS-Kly-110:PwrRfl-"
macros["AI04"] = "RFS-Cav-110:PwrFwd-"
macros["AI05"] = "RFS-Cav-120:PwrFwd-"
macros["AI06"] = "RFS-Load-120:PwrFwd-"
macros["AI07"] = "RFS-Cav-110:Fld-"
macros["AI08"] = "RFS-Cav-110:PwrRfl-"
macros["AI09"] = "RFS-Cav-120:PwrRfl-"
macros["AI10"] = "RFS-Mod-110:Vol-"
macros["AI11"] = "RFS-Mod-110:Cur-"
macros["AI12"] = "RFS-SolPS-110:Cur-"
macros["AI13"] = "RFS-SolPS-120:Cur-"
macros["AI14"] = "RFS-SolPS-130:Cur-"
macros["AI15"] = "RFS-FIM-101:AI6-"
macros["AI16"] = "RFS-FIM-101:AI7-"
macros["AI17"] = "RFS-FIM-101:AI8-"
macros["AI18"] = "RFS-FIM-101:AI9-"
macros["AI19"] = "RFS-FIM-101:AI10-"
macros["DI00"] = "RFS-SIM-110:HvEna-"
macros["DI01"] = "RFS-SIM-110:RfEna-"
macros["DI02"] = "RFS-Mod-110:PCcon-"
macros["DI03"] = "RFS-Mod-110:Ready-"
macros["DI04"] = "RFS-VacPS-110:I-SP-"
macros["DI05"] = "RFS-VacPS-120:I-SP-"
macros["DI06"] = "RFS-FIM-101:DI7-"
macros["DI07"] = "RFS-FIM-101:DI8-"
macros["DI08"] = "RFS-VacMon-110:Status-"
macros["DI09"] = "RFS-FIM-101:DI10-"
macros["DI10"] = "RFS-VacMon-130:Status-"
macros["DI11"] = "RFS-FIM-101:DI12-"
macros["DI12"] = "RFS-FIM-101:DI13-"
macros["DI13"] = "RFS-FIM-101:DI14-"
macros["DI14"] = "RFS-ADG-110:IlckStatus-"
macros["DI15"] = "RFS-ADG-110:PwrFail-"
macros["DI16"] = "RFS-FIM-101:DI17-"
macros["DI17"] = "RFS-FIM-101:DI18-"
macros["DI18"] = "RFS-FIM-101:DI19-"
macros["DI19"] = "RFS-FIM-101:DI20-"
macros["DI20"] = "RFS-LLRF-101:Status-"
macros["RP0"] = "RFS-FIM-101:RP1"
macros["RP1"] = "RFS-FIM-101:RP2"
macros["CD0"] = "RFS-FIM-101:CD1"
macros["CD1"] = "RFS-FIM-101:CD2"
 
##################################################################################################
# Construct the macros:
##################################################################################################
 
for k,v in macros.items():
    widget.getPropertyValue("macros").add(k,v)
 
