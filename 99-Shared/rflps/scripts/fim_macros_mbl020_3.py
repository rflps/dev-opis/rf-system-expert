from org.csstudio.opibuilder.scriptUtil import PVUtil
 
# This script will populate several macros that will be used to mount the PV names inside the GUIs.
# Version: 2021-10-20
 
##################################################################################################
# Macro data:
##################################################################################################
 
macros = {}
 
macros["P"] = "MBL-020RFC:"
macros["SEC"] = "MBL"
macros["SUB"] = "020RFC"
macros["IOC_"] = "RFS-FIM-301:"
macros["IOCSTATS_"] = "prefix_+ioc2_"
macros["AI00"] = "RFS-Mod-310:Vol-"
macros["AI01"] = "RFS-Mod-310:Cur-"
macros["AI02"] = "RFS-SolPS-310:Cur-"
macros["AI03"] = "RFS-SolPS-320:Cur-"
macros["AI04"] = "RFS-FIM-301:AI5-"
macros["AI05"] = "RFS-EPR-310:Cur-"
macros["AI06"] = "RFS-FIM-301:AI7-"
macros["AI07"] = "RFS-FIM-301:AI8-"
macros["AI08"] = "RFS-FIM-301:AI9-"
macros["AI09"] = "RFS-FIM-301:AI10-"
macros["AI10"] = "RFS-PAmp-310:PwrFwd-"
macros["AI11"] = "RFS-Kly-310:PwrFwd-"
macros["AI12"] = "RFS-Load-320:PwrFwd-"
macros["AI13"] = "RFS-Kly-310:PwrRfl-"
macros["AI14"] = "RFS-Cav-310:PwrFwd-"
macros["AI15"] = "RFS-Cav-310:PwrRfl-"
macros["AI16"] = "RFS-Load-320:PwrRfl-"
macros["AI17"] = "RFS-Cav-310:Fld-"
macros["AI18"] = "RFS-FIM-301:RF9-"
macros["AI19"] = "RFS-FIM-301:RF10-"
macros["DI00"] = "RFS-SIM-310:HvEna-"
macros["DI01"] = "RFS-SIM-310:RfEna-"
macros["DI02"] = "RFS-Mod-310:PCcon-"
macros["DI03"] = "RFS-Mod-310:Ready-"
macros["DI04"] = "RFS-VacPS-310:I-SP-"
macros["DI05"] = "RFS-VacPS-320:I-SP-"
macros["DI06"] = "RFS-FIM-301:DI7-"
macros["DI07"] = "RFS-FIM-301:DI8-"
macros["DI08"] = "RFS-VacCav-310:Status-"
macros["DI09"] = "RFS-FIM-301:DI10-"
macros["DI10"] = "RFS-VacBody-310:Status-"
macros["DI11"] = "RFS-FIM-301:DI12-"
macros["DI12"] = "RFS-FIM-301:DI13-"
macros["DI13"] = "RFS-FIM-301:DI14-"
macros["DI14"] = "RFS-ADG-310:IlckStatus-"
macros["DI15"] = "RFS-ADG-310:PwrFail-"
macros["DI16"] = "RFS-FIM-301:DI17-"
macros["DI17"] = "RFS-FIM-301:DI18-"
macros["DI18"] = "RFS-FIM-301:DI19-"
macros["DI19"] = "RFS-FIM-301:DI20-"
macros["DI20"] = "RFS-LLRF-301:Status-"
macros["RP0"] = "RFS-FIM-301:RP1"
macros["RP1"] = "RFS-FIM-301:RP2"
macros["CD0"] = "RFS-FIM-301:CD1"
macros["CD1"] = "RFS-FIM-301:CD2"
 
##################################################################################################
# Construct the macros:
##################################################################################################
 
for k,v in macros.items():
    widget.getPropertyValue("macros").add(k,v)
 
