from org.csstudio.opibuilder.scriptUtil import PVUtil
 
# This script will populate several macros that will be used to mount the PV names inside the GUIs.
# Version: 2021-10-20
 
##################################################################################################
# Macro data:
##################################################################################################
 
macros = {}
 
macros["P"] = "MBL-030RFC:"
macros["SEC"] = "MBL"
macros["SUB"] = "030RFC"
macros["IOC_"] = "RFS-FIM-201:"
macros["IOCSTATS_"] = "prefix_+ioc2_"
macros["AI00"] = "RFS-Mod-210:Vol-"
macros["AI01"] = "RFS-Mod-210:Cur-"
macros["AI02"] = "RFS-SolPS-210:Cur-"
macros["AI03"] = "RFS-SolPS-220:Cur-"
macros["AI04"] = "RFS-FIM-201:AI5-"
macros["AI05"] = "RFS-EPR-210:Cur-"
macros["AI06"] = "RFS-FIM-201:AI7-"
macros["AI07"] = "RFS-FIM-201:AI8-"
macros["AI08"] = "RFS-FIM-201:AI9-"
macros["AI09"] = "RFS-FIM-201:AI10-"
macros["AI10"] = "RFS-PAmp-210:PwrFwd-"
macros["AI11"] = "RFS-Kly-210:PwrFwd-"
macros["AI12"] = "RFS-Load-220:PwrFwd-"
macros["AI13"] = "RFS-Kly-210:PwrRfl-"
macros["AI14"] = "RFS-Cav-210:PwrFwd-"
macros["AI15"] = "RFS-Cav-210:PwrRfl-"
macros["AI16"] = "RFS-Load-220:PwrRfl-"
macros["AI17"] = "RFS-Cav-210:Fld-"
macros["AI18"] = "RFS-FIM-201:RF9-"
macros["AI19"] = "RFS-FIM-201:RF10-"
macros["DI00"] = "RFS-SIM-210:HvEna-"
macros["DI01"] = "RFS-SIM-210:RfEna-"
macros["DI02"] = "RFS-Mod-210:PCcon-"
macros["DI03"] = "RFS-Mod-210:Ready-"
macros["DI04"] = "RFS-VacPS-210:I-SP-"
macros["DI05"] = "RFS-VacPS-220:I-SP-"
macros["DI06"] = "RFS-FIM-201:DI7-"
macros["DI07"] = "RFS-FIM-201:DI8-"
macros["DI08"] = "RFS-VacCav-210:Status-"
macros["DI09"] = "RFS-FIM-201:DI10-"
macros["DI10"] = "RFS-VacBody-210:Status-"
macros["DI11"] = "RFS-FIM-201:DI12-"
macros["DI12"] = "RFS-FIM-201:DI13-"
macros["DI13"] = "RFS-FIM-201:DI14-"
macros["DI14"] = "RFS-ADG-210:IlckStatus-"
macros["DI15"] = "RFS-ADG-210:PwrFail-"
macros["DI16"] = "RFS-FIM-201:DI17-"
macros["DI17"] = "RFS-FIM-201:DI18-"
macros["DI18"] = "RFS-FIM-201:DI19-"
macros["DI19"] = "RFS-FIM-201:DI20-"
macros["DI20"] = "RFS-LLRF-201:Status-"
macros["RP0"] = "RFS-FIM-201:RP1"
macros["RP1"] = "RFS-FIM-201:RP2"
macros["CD0"] = "RFS-FIM-201:CD1"
macros["CD1"] = "RFS-FIM-201:CD2"
 
##################################################################################################
# Construct the macros:
##################################################################################################
 
for k,v in macros.items():
    widget.getPropertyValue("macros").add(k,v)
 
