from org.csstudio.opibuilder.scriptUtil import PVUtil
 
# This script will populate several macros that will be used to mount the PV names inside the GUIs.
# Version: 2021-10-20
 
##################################################################################################
# Macro data:
##################################################################################################
 
macros = {}
 
macros["P"] = "TS2-RFLPS2"
macros["SEC"] = "TS2"
macros["SUB"] = "RFLPS2"
macros["IOC_"] = "RFS-FIM-110:"
macros["IOCSTATS_"] = "prefix_+ioc2_"
macros["AI00"] = "RFS-DirC-110:PwrFwdDA-"
macros["AI01"] = "RFS-DirC-110:PwrFwdPA-"
macros["AI02"] = "RFS-DirC-110:PwrFwdL2-"
macros["AI03"] = "RFS-DirC-110:PwrRflctPA-"
macros["AI04"] = "RFS-DirC-110:PwrFwdC1-"
macros["AI05"] = "RFS-DirC-110:PwrFwdC2-"
macros["AI06"] = "RFS-DirC-110:PwrFwdL1-"
macros["AI07"] = "RFS-DirC-110:PwrRflctL2-"
macros["AI08"] = "RFS-DirC-110:PwrRflctC1-"
macros["AI09"] = "RFS-DirC-110:PwrRflctC2-"
macros["AI10"] = "RFS-Mod-110:Cur-"
macros["AI11"] = "RFS-Mod-110:Vol-"
macros["AI12"] = "RFS-SolPS-110:Cur-"
macros["AI13"] = "RFS-SolPS-120:Cur-"
macros["AI14"] = "RFS-SolPS-130:Cur-"
macros["AI15"] = "RFS-EPU-110:Cur-"
macros["AI16"] = "RFS-EPU-120:Cur-"
macros["AI17"] = "RFS-FIM-110:AI8-"
macros["AI18"] = "RFS-FIM-110:AI9-"
macros["AI19"] = "RFS-FIM-110:AI10-"
macros["DI00"] = "RFS-SIM-110:HvEnaCmd-"
macros["DI01"] = "RFS-SIM-110:RfEnaCmd-"
macros["DI02"] = "RFS-SIM-110:PConn-"
macros["DI03"] = "RFS-Mod-110:Fault-"
macros["DI04"] = "RFS-VacPS-110:I-SP-"
macros["DI05"] = "RFS-VacPS-120:I-SP-"
macros["DI06"] = "RFS-ADR-110:ItlckStat-"
macros["DI07"] = "RFS-FIM-110:DI8-"
macros["DI08"] = "RFS-CavVac-110:ItckStat-"
macros["DI09"] = "RFS-CavVac-120:ItckStat-"
macros["DI10"] = "RFS-ADR-201003:ItckStat-"
macros["DI11"] = "RFS-ADR-201004:ItckStat-"
macros["DI12"] = "RFS-ADR-SCU-110:ItckStat-"
macros["DI13"] = "RFS-ADR-SCU-120:ItckStat-"
macros["DI14"] = "RFS-FIM-110:DI15-"
macros["DI15"] = "RFS-FIM-110:DI16-"
macros["DI16"] = "RFS-FIM-110:DI17-"
macros["DI17"] = "RFS-FIM-110:DI18-"
macros["DI18"] = "RFS-FIM-110:DI19-"
macros["DI19"] = "RFS-FIM-110:DI20"
macros["DI20"] = "RFS-LLRF-101:Status-"
macros["RP0"] = "RFS-FIM-110:RP1"
macros["RP1"] = "RFS-FIM-110:RP2"
macros["CD0"] = "RFS-FIM-110:CD1"
macros["CD1"] = "RFS-FIM-110:CD2"
 
##################################################################################################
# Construct the macros:
##################################################################################################
 
for k,v in macros.items():
    widget.getPropertyValue("macros").add(k,v)
 
