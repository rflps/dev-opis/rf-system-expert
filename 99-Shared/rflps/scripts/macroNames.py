from org.csstudio.opibuilder.scriptUtil import PVUtil
 
# This script will populate several macros that will be used to mount the PV names inside the GUIs.
# Version: 2021-10-20
 
##################################################################################################
# Macro data:
##################################################################################################
 
macros = {}
 
macros["PREFIX"] = "PVUtil.getString(pvs[0])"
macros["SEC"] = "PVUtil.getString(pvs[1])"
macros["SUB"] = "PVUtil.getString(pvs[2])"
macros["IOC_"] = "FIM-110"
macros["AI00"] = "Mod-110:Cur"
macros["AI01"] = "Mod-110:Vol"
macros["AI02"] = "SolPS-110:Cur"
macros["AI03"] = "SolPS-120:Cur"
macros["AI04"] = "SolPS-130:Cur"
macros["AI05"] = "FIM-110:AI5"
macros["AI06"] = "FIM-110:AI6"
macros["AI07"] = "FIM-110:AI7"
macros["AI08"] = "FIM-110:AI8"
macros["AI09"] = "FIM-110:AI9"
macros["AI10"] = "DirC-110:DaPwrFwdDA"
macros["AI11"] = "DirC-110:PwrFwdKly"
macros["AI12"] = "DirC-110:PwrFwdLoad"
macros["AI13"] = "DirC-110:PwrRefKly"
macros["AI14"] = "FIM-110:AI14"
macros["AI15"] = "FIM-110:AI15"
macros["AI16"] = "FIM-110:AI16"
macros["AI17"] = "DirC-110:PwrRefLoad"
macros["AI18"] = "FIM-110:AI18"
macros["AI19"] = "FIM-110:AI19"
macros["DI00"] = "SIM-110:HvEnaCmd"
macros["DI01"] = "SIM-110:RfEnaCmd"
macros["DI02"] = "SIM-110:PCconnect"
macros["DI03"] = "Mod-110:Fault"
macros["DI04"] = "VacPS-110:I-SP"
macros["DI05"] = "VacPS-120:I-SP"
macros["DI06"] = "ADR-110:ItlckStat"
macros["DI07"] = "FIM-110:DI7"
macros["DI08"] = "ADR-201001:ItckStat"
macros["DI09"] = "ADR-201002:ItckStat"
macros["DI10"] = "ADR-201003:ItckStat"
macros["DI11"] = "ADR-201004:ItckStat"
macros["DI12"] = "ADR-201005:ItckStat"
macros["DI13"] = "FIM-110:DI13"
macros["DI14"] = "FIM-110:DI14"
macros["DI15"] = "FIM-110:DI15"
 
##################################################################################################
# Construct the macros:
##################################################################################################
 
for k,v in macros.items():
    widget.getPropertyValue("macros").add(k,v)
 
