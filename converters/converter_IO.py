import os

def _pathfix(path):
    return '/'.join(path.split('/99-Shared/'))

def _writeFull(mainDict):
    # print(mainDict)
    for tab,v1 in mainDict.items():
        print('--------------------')
        print('Tab name: {}'.format(tab))
        lines = _writeMainPy_dynamic(_writeMainPy_std01(),v1,tab)
        lines = _writeMainPy_std02(lines,v1)
        fname = '_main_macros_{}'.format(tab_fix(tab))

        _create_file(tab_fix(tab),fname,lines)

    # _printEmbeddedCode(mainDict)

def tab_fix(tab):
    if len(tab.split(' '))>1:
        tab = '{}_{}'.format(tab.split(' ')[0],tab.split(' ')[-1])
    return tab

def fname_fix(fname):
    return '_'.join(fname.split('-'))

def _writeSubPy(tab,fname,macrodict):
    # Writing the [OPIname]_macros_[section].py file
    lines = [('def get():')]
    lines.append('    {} = {{}}'.format(fname))
    for k,v in macrodict.items():
        lines.append("    {}['{}']='{}'".format(fname,k,v))
    lines.append('    return {}'.format(fname))
    _create_file(tab,fname,lines)

def _printEmbeddedCode(mainDict):
    print('embedded code')

    for tab,v1 in mainDict.items():

        print('            <tab>')
        print('              <name>{}</name>'.format(tab_fix(tab)))
        print('              <children>')
        print('                <widget type="embedded" version="2.0.0">')
        print('                  <name>{}</name>'.format(tab_fix(tab)))
        print('                  <macros>')
        print('                    <section>{}/_main_macros_{}.py</section>'.format(tab_fix(tab),tab_fix(tab)))
        print('                  </macros>')
        print('                  <file>../99-Shared/overview/main.bob</file>')
        print('                  <x>20</x>')
        print('                  <y>20</y>')
        print('                  <width>1160</width>')
        print('                  <height>540</height>')
        print('                </widget>')
        print('              </children>')
        print('            </tab>')

def _create_file(tab,fname,lines):
    try:
        os.mkdir('conversion')
    except Exception:
        pass
    try:
        os.mkdir(os.path.join('conversion',tab_fix(tab)))
    except Exception:
        pass
    f = open(os.path.join('conversion',tab_fix(tab),'{}.py'.format(fname.split('/')[-1])),'w')
    for l in lines:
        f.write('{}\n'.format(l))
    f.close()

def _writeMainPy_std01():
    lines = []
    lines.append("from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil")
    lines.append("from org.phoebus.framework.macros import MacroHandler")
    lines.append("")
    lines.append("# Section and subsection are fixed per script")
    lines.append("macros = {}")
    lines.append("")
    lines.append("##################################################################################################")
    lines.append("#	Create the list of all buttons")
    lines.append("##################################################################################################")
    lines.append("")
    lines.append("buttons_lst = [ 'LPSdetailed',")
    lines.append("                'LPSoverview',")
    lines.append("                'TimingLocalMode',")
    lines.append("                'LLRF',")
    lines.append("                'KlyInfo',")
    lines.append("                'SignalConditioning',")
    lines.append("                'ElectronPickup',")
    lines.append("                'PinDiode',")
    lines.append("                'LocalOscillator',")
    lines.append("                'Preamp',")
    lines.append("                'Solenoid',")
    lines.append("                'IonPump',")
    lines.append("                'FilamentPower',")
    lines.append("                'Circulator',")
    lines.append("                'ArcDetectors',")
    lines.append("                'RFoverview',")
    lines.append("                'FullOverview']")
    lines.append("")
    lines.append("# Define enabled and disabled buttons")
    lines.append("btns_enabled = {}")
    lines.append("for btn in buttons_lst:")
    lines.append("    btns_enabled[btn]='true'")
    lines.append("")
    lines.append("## Buttons Disabled")
    return lines

def _writeMainPy_std02(lines,sectionDict):
    lines.append("")
    lines.append("##################################################################################################")
    lines.append("# Send the macros to each respective widget, or disable the buttons")
    lines.append("##################################################################################################")
    lines.append("")
    lines.append("buttons = {}")
    lines.append("groups = {}")
    lines.append("for btn in buttons_lst:")
    lines.append("    buttons['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, '{}_btn'.format(btn))")
    lines.append("    groups['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, btn)")
    lines.append("for btn,enabled in btns_enabled.items():")
    lines.append("    if enabled=='false':")
    lines.append("        buttons['{}_button'.format(btn)].setPropertyValue('enabled','false')")
    lines.append("    if enabled=='true':")
    lines.append("        for k,v in macros[btn].items():")
    lines.append("            groups['{}_button'.format(btn)].getPropertyValue('macros').add(k,v)")
    return lines

def _writeMainPy_dynamic(lines,sectionDict,tab):
    btn_names = {}
    btn_names['rflps']      = "LPSdetailed"
    btn_names['overview']   = "LPSoverview"
    btn_names['timing']     = "TimingLocalMode"
    btn_names['llrf']       = "LLRF"
    btn_names['klystron']   = "KlyInfo"
    if tab.split('-')[0] not in ['RFQ','DTL']:
        lines.append("btns_enabled['KlyInfo']='false'")
    lines.append("btns_enabled['LPSoverview']='false'")
    lines.append("btns_enabled['SignalConditioning']='false'")
    lines.append("btns_enabled['ElectronPickup']='false'")
    lines.append("btns_enabled['PinDiode']='false'")
    lines.append("btns_enabled['LocalOscillator']='false'")
    lines.append("btns_enabled['Preamp']='false'")
    lines.append("btns_enabled['Solenoid']='false'")
    lines.append("btns_enabled['IonPump']='false'")
    lines.append("btns_enabled['FilamentPower']='false'")
    lines.append("btns_enabled['Circulator']='false'")
    lines.append("btns_enabled['ArcDetectors']='false'")
    lines.append("btns_enabled['RFoverview']='false'")
    lines.append("btns_enabled['FullOverview']='false'")
    lines.append("")
    lines.append("##################################################################################################")
    lines.append("#	Hardcoded macro data")
    lines.append("##################################################################################################")
    lines.append("")
    lines.append("# Macros for the buttons")

    for k2,v2 in sectionDict.items():
        specie = v2['path'].split('/')[-2]
        fname = '_{}_macros_{}'.format(specie,tab_fix(tab))
        lines.append("## Macros for {}".format(specie))
        lines.append("import {}".format(fname_fix(fname)))
        lines.append("macros['{}'] = {}.get()".format(btn_names[specie],fname_fix(fname)))
        lines.append("")

        _writeSubPy(tab_fix(tab),fname_fix(fname),v2)
    return lines
