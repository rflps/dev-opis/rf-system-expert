import xmltodict
from copy import deepcopy
from converter_IO import _writeFull, _pathfix
f = 'top-rf__dev.bob'
with open(f) as fd:
    doc = xmltodict.parse(fd.read())

# Widget 0 is the tabs widget
maintabs = doc['display']['widget'][0]['tabs']['tab'] # 3 tabs in list item

mainDict = {}

for i in range(len(maintabs)):
    subtabs = maintabs[i]['children']['widget']['tabs']['tab']
    for j in range(len(subtabs)):
        subsubtabs = subtabs[j]
        tabname = deepcopy(subsubtabs['name'])
        objects = subsubtabs['children']['widget']
        for k in range(len(objects)):
            obj = objects[k]
            if obj['@type'] == 'group' and 'widget' in obj.keys():
                children = obj['widget']
                if isinstance(children, list):
                    for l in range(len(children)):
                        # print(i,j,k,l)
                        child = children[l]
                        if isinstance(child, dict):
                            # Nothing but dict items found
                            if child['@type'] == 'action_button':
                                actions = child['actions']['action']
                                if isinstance(actions, dict):
                                    if 'macros' in actions.keys():
                                        if tabname not in mainDict.keys():
                                            mainDict[tabname] = {}
                                        mainDict[tabname][child['name']] = actions['macros']
                                        mainDict[tabname][child['name']]['path'] = _pathfix(actions['file'])

                            # print(child.keys())

                if isinstance(children, dict):
                    if children['@type'] == 'action_button':
                        actions = children['actions']['action']
                        if isinstance(actions, dict):
                            if 'macros' in actions.keys():
                                if tabname not in mainDict.keys():
                                    mainDict[tabname] = {}
                                mainDict[tabname][child['name']] = actions['macros']
                                mainDict[tabname][child['name']]['path'] = _pathfix(actions['file'])

# Now mainDict contains tabs and buttons belonging to each tab,
# with macros being the values as orderedDicts


_writeFull(mainDict)
