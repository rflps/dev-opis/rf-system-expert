import os
files = [   'fim_macros_rfq_01.py',
            'macroNames.py',
            'fim_macros_ts2_02.py',
            'fim_macros_ts2_01.py',
            'fim_macros_spk_01.py',
            'fim_macros_mbl030_4.py',
            'fim_macros_mbl030_3.py',
            'fim_macros_mbl030_2.py',
            'fim_macros_mbl030_1.py',
            'fim_macros_mbl020_4.py',
            'fim_macros_mbl020_3.py',
            'fim_macros_mbl020_2.py',
            'fim_macros_mbl020_1.py',
            'fim_macros_lab_11.py',
            'fim_macros_lab_10.py',
            'fim_macros_dtl_050.py',
            'fim_macros_dtl_040.py',
            'fim_macros_dtl_030.py',
            'fim_macros_dtl_010.py']



def removeSpacing(x):
    return ''.join(x.split(' '))

def removeQuotes(x):
    return ''.join(x.split('"'))

for file in files:
    d1,d2 = {},{}
    for fl in open(os.path.join('fimpys',file), "r").read().split('\n'):
        if len(fl.split('#')[0]) > 0 and len(fl.split('='))>1:
            l = removeQuotes(removeSpacing(fl)).split('=')
            d1[l[0]] = l[1]
        elif len(fl.split('getPropertyValue("macros").add(')) > 1:
            # widget.getPropertyValue("macros").add("CD1", cd01_)
            l = removeQuotes(removeSpacing(fl.split('getPropertyValue("macros").add(')[1].split(')')[0])).split(',')
            d2[l[0]] = l[1]

    # d2 dictates :
    macros = {}
    for k,v in d2.items():
        macros[k] = d1[v]

    output = []
    ht = '##################################################################################################'
    output.append(['from org.csstudio.opibuilder.scriptUtil import PVUtil',' ',
                   '# This script will populate several macros that will be used to mount the PV names inside the GUIs.',
                   '# Version: 2021-10-20',
                   ' '])
    output.append([ht,'# Macro data:',ht,' ','macros = {}',' '])
    output.append([])
    output.append([' ',ht,'# Construct the macros:',ht,' '])
    output.append(['for k,v in macros.items():','    widget.getPropertyValue("macros").add(k,v)',' '])

    for k,v in macros.items():
        output[2].append('macros["{}"] = "{}"'.format(k,v))

    try:
        os.mkdir('converted')
    except Exception:
        pass
    f = open(os.path.join('converted',file),'w')
    for l1 in output:
        for l2 in l1:
            f.write('{}\n'.format(l2))
    f.close()
